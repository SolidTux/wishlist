mod server;
mod wishlist;

use bcrypt::DEFAULT_COST;
use std::error::Error;
use structopt::StructOpt;
use tokio::runtime::Runtime;
use wishlist::*;

#[derive(Debug, StructOpt)]
#[structopt(author, about)]
enum Opt {
    /// User management.
    User(UserOpt),
    /// Run web server.
    Server {
        /// Port at which the server listens.
        #[structopt(default_value = "8080")]
        port: u16,
        /// Make output quite.
        #[structopt(short = "q", long = "quiet")]
        quiet: bool,
        /// Loggin verbosity.
        #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
        verbose: usize,
    },
}

#[derive(Debug, StructOpt)]
enum UserOpt {
    /// Add user.
    Add {
        /// Username.
        user: String,
    },
    /// Remove user.
    Remove {
        /// Username.
        user: String,
    },
    /// Check user password.
    Check {
        /// Username.
        user: String,
    },
    /// Update user password.
    Update {
        /// Username.
        user: String,
    },
    /// List users.
    List,
}

fn main() -> Result<(), Box<dyn Error>> {
    let opt = Opt::from_args();
    let mut db = Database::load().unwrap_or_default();
    match opt {
        Opt::User(user_opt) => match user_opt {
            UserOpt::Add { user } => {
                if db.has_user(&user) {
                    println!("User already exists.");
                } else {
                    let mut password = rpassword::prompt_password_stdout("Password: ")?;
                    loop {
                        let retype = rpassword::prompt_password_stdout("Retype password: ")?;
                        if password == retype {
                            break;
                        } else {
                            println!("Passwords do not match.");
                            password = rpassword::prompt_password_stdout("Password: ")?;
                        }
                    }
                    let hash = bcrypt::hash(password, DEFAULT_COST)?;
                    db.add_user(user, hash);
                    db.save()?;
                }
            }
            UserOpt::Update { user } => {
                if !db.has_user(&user) {
                    println!("User does not exist.");
                } else {
                    let mut password = rpassword::prompt_password_stdout("Password: ")?;
                    loop {
                        let retype = rpassword::prompt_password_stdout("Retype password: ")?;
                        if password == retype {
                            break;
                        } else {
                            println!("Passwords do not match.");
                            password = rpassword::prompt_password_stdout("Password: ")?;
                        }
                    }
                    let hash = bcrypt::hash(password, DEFAULT_COST)?;
                    db.update_user(user, hash);
                    db.save()?;
                }
            }
            UserOpt::Remove { user } => {
                if !db.has_user(&user) {
                    println!("User does not exist.");
                } else {
                    db.remove_user(&user);
                    db.save()?;
                }
            }
            UserOpt::Check { user } => {
                if !db.has_user(&user) {
                    println!("User does not exist.");
                } else {
                    let password = rpassword::prompt_password_stdout("Password: ")?;
                    if db.check_user(&user, password)? {
                        println!("Password correct.");
                    } else {
                        println!("Password wrong.");
                    }
                }
            }
            UserOpt::List => {
                for user in db.users {
                    println!("{}", user.name);
                }
            }
        },
        Opt::Server {
            port,
            quiet,
            verbose,
        } => {
            stderrlog::new().quiet(quiet).verbosity(verbose).init()?;
            let mut rt = Runtime::new()?;
            rt.block_on(server::run(db, port));
        }
    }
    Ok(())
}
