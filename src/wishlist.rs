use bcrypt::BcryptError;
use directories::ProjectDirs;
use serde::{Deserialize, Serialize};
use std::{
    collections::BTreeMap,
    error::Error,
    fmt::{self, Display, Formatter},
    fs::{self, File},
};

pub type DbResult<T> = std::result::Result<T, DbError>;

#[derive(Debug)]
pub enum DbError {
    UserNotFound(String),
    CryptoError(BcryptError),
}

impl Display for DbError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            DbError::UserNotFound(u) => write!(f, "User \"{}\" not found.", u),
            DbError::CryptoError(e) => write!(f, "Crypto error: {}", e),
        }
    }
}

impl Error for DbError {}

impl From<BcryptError> for DbError {
    fn from(e: BcryptError) -> Self {
        DbError::CryptoError(e)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord)]
pub struct Item {
    pub name: String,
    pub bought_by: Option<String>,
}

impl Item {
    pub fn new(name: &str) -> Self {
        Item {
            name: name.to_string(),
            bought_by: None,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Wishlist {
    pub name: String,
    pub user: String,
    pub items: BTreeMap<String, Item>,
}

impl Wishlist {
    pub fn new(user: &str, name: &str) -> Self {
        Wishlist {
            name: name.to_string(),
            user: user.to_string(),
            items: BTreeMap::new(),
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct User {
    pub name: String,
    hash: String,
}

impl User {
    pub fn new(name: String, hash: String) -> Self {
        User { name, hash }
    }

    pub fn check(&self, password: String) -> DbResult<bool> {
        bcrypt::verify(password, &self.hash).map_err(|e| e.into())
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct Database {
    pub wishlists: BTreeMap<String, Wishlist>,
    pub users: Vec<User>,
}

impl Database {
    pub fn load() -> Result<Self, Box<dyn Error>> {
        let project_dirs = ProjectDirs::from("de", "solidtux", "wishlist").unwrap();
        let mut path = project_dirs.data_dir().to_path_buf();
        path.push("db.json");
        let f = File::open(path)?;
        let db = serde_json::from_reader(f)?;
        Ok(db)
    }

    pub fn save(&self) -> Result<(), Box<dyn Error>> {
        let project_dirs = ProjectDirs::from("de", "solidtux", "wishlist").unwrap();
        let mut path = project_dirs.data_dir().to_path_buf();
        fs::create_dir_all(&path)?;
        path.push("db.json");
        let f = File::create(path)?;
        serde_json::to_writer(f, self)?;
        Ok(())
    }

    pub fn has_user(&mut self, user: &str) -> bool {
        self.users.iter().find(|x| x.name == user).is_some()
    }

    pub fn add_user(&mut self, user: String, hash: String) {
        if self.users.iter().find(|x| x.name == user).is_some() {
            println!("User already exists.");
        } else {
            self.users.push(User::new(user, hash));
        }
    }

    pub fn remove_user(&mut self, user: &str) {
        match self.users.iter().position(|x| x.name == user) {
            Some(i) => {
                self.users.remove(i);
            }
            None => println!("User does not exist."),
        }
    }

    pub fn update_user(&mut self, user: String, hash: String) {
        match self.users.iter_mut().find(|x| x.name == user) {
            Some(u) => u.hash = hash,
            None => println!("User does not exist."),
        }
    }

    pub fn check_user(&self, user: &str, password: String) -> Result<bool, DbError> {
        match self.users.iter().find(|x| &x.name == user) {
            Some(u) => {
                let res = u.check(password)?;
                Ok(res)
            }
            None => Err(DbError::UserNotFound(user.to_string())),
        }
    }
}
