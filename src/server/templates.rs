use crate::wishlist::Wishlist;
use askama::Template;

#[derive(Template)]
#[template(path = "index.html")]
pub struct IndexTemplate {
    pub user: String,
    pub wishlists: Vec<(String, Wishlist)>,
}

#[derive(Template)]
#[template(path = "error.html")]
pub struct ErrorTemplate {
    pub error: String,
}

#[derive(Template)]
#[template(path = "list.html")]
pub struct ListTemplate {
    pub id: String,
    pub user: String,
    pub list: Wishlist,
}

#[derive(Template)]
#[template(path = "login.html")]
pub struct LoginTemplate {
    pub referer: Option<String>,
}
