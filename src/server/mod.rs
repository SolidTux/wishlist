mod templates;

use self::templates::*;
use crate::wishlist::{Database, Item, Wishlist};
use askama::Template;
use log::error;
use ring::{
    error::Unspecified,
    rand::{SecureRandom, SystemRandom},
};
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    convert::Infallible,
    fmt::{self, Display, Formatter},
    sync::Arc,
};
use tokio::sync::Mutex;
use warp::{http::StatusCode, reject::Reject, Filter, Rejection, Reply};

#[derive(Debug, Clone, Serialize)]
struct State {
    db: Database,
    tokens: HashMap<String, String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct LoginForm {
    user: String,
    password: String,
    referer: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct AddListForm {
    name: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct AddItemForm {
    name: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct IndexParameters {
    referer: Option<String>,
}

fn random_token() -> Result<String, Unspecified> {
    let rng = SystemRandom::new();
    let mut token = [0u8; 8];
    rng.fill(&mut token)?;
    let mut res = String::new();
    for x in &token {
        res += &format!("{:02X}", x);
    }
    Ok(res)
}

fn temporary_redirect(path: &str) -> impl Reply {
    warp::reply::with_header(
        warp::reply::with_status("", StatusCode::FOUND),
        "Location",
        path,
    )
}

async fn debug(state: Arc<Mutex<State>>) -> Result<impl Reply, Rejection> {
    let state = state.lock().await;
    Ok(warp::reply::json(&(*state)))
}

#[derive(Debug, Clone, Copy)]
enum Error {
    Forbidden,
    Internal,
}

impl Error {
    fn status(&self) -> StatusCode {
        match self {
            Error::Internal => StatusCode::INTERNAL_SERVER_ERROR,
            Error::Forbidden => StatusCode::FORBIDDEN,
        }
    }
}

impl Reject for Error {}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        f.write_str(match self {
            Error::Internal => "Internal Server Error",
            Error::Forbidden => "Forbidden",
        })
    }
}

async fn login(state: Arc<Mutex<State>>, form: LoginForm) -> Result<impl Reply, Rejection> {
    let mut state = state.lock().await;
    let valid = state
        .db
        .check_user(&form.user, form.password)
        .map_err(|_| warp::reject::reject())?;
    if valid {
        let t = random_token().map_err(|_| warp::reject::custom(Error::Internal))?;
        state.tokens.insert(t.clone(), form.user);
        Ok(warp::reply::with_header(
            temporary_redirect(&form.referer.unwrap_or("/".to_string())),
            "Set-Cookie",
            format!("token={}; Max-Age=3600", t),
        ))
    } else {
        Ok(warp::reply::with_header(
            temporary_redirect(&form.referer.unwrap_or("/".to_string())),
            "Set-Cookie",
            "token=; Max-Age=0",
        ))
    }
}

async fn logout(state: Arc<Mutex<State>>, token: Option<String>) -> Result<impl Reply, Rejection> {
    if let Some(t) = token {
        let mut state = state.lock().await;
        state.tokens.remove(&t);
    }
    Ok(warp::reply::with_header(
        temporary_redirect("/"),
        "Set-Cookie",
        "token=; Max-Age=0",
    ))
}

async fn index(
    state: Arc<Mutex<State>>,
    token: Option<String>,
    parameters: IndexParameters,
) -> Result<Box<dyn Reply>, Rejection> {
    let state = state.lock().await;
    match parameters.referer {
        Some(referer) => Ok(Box::new(warp::reply::html(
            LoginTemplate {
                referer: Some(referer),
            }
            .render()
            .map_err(|_| warp::reject::custom(Error::Internal))?,
        ))),
        None => match token {
            Some(token) => match state.tokens.get(&token) {
                Some(user) => Ok(Box::new(warp::reply::html(
                    IndexTemplate {
                        user: user.clone(),
                        wishlists: state
                            .db
                            .wishlists
                            .clone()
                            .into_iter()
                            .filter(|(_, b)| &b.user == user)
                            .collect(),
                    }
                    .render()
                    .map_err(|_| warp::reject::custom(Error::Internal))?,
                ))),
                None => Ok(Box::new(warp::reply::with_header(
                    temporary_redirect("/"),
                    "Set-Cookie",
                    "token=; Max-Age=0",
                ))),
            },
            None => Ok(Box::new(warp::reply::html(
                LoginTemplate { referer: None }
                    .render()
                    .map_err(|_| warp::reject::custom(Error::Internal))?,
            ))),
        },
    }
}

async fn list(
    (state, user): (Arc<Mutex<State>>, Option<String>),
    id: String,
) -> Result<Box<dyn Reply>, Rejection> {
    let state = state.lock().await;
    if let Some(u) = user {
        if let Some(list) = state.db.wishlists.get(&id) {
            Ok(Box::new(warp::reply::html(
                ListTemplate {
                    id,
                    user: u,
                    list: list.clone(),
                }
                .render()
                .map_err(|_| warp::reject::custom(Error::Internal))?,
            )))
        } else {
            return Err(warp::reject::not_found());
        }
    } else {
        Ok(Box::new(temporary_redirect(&format!(
            "/?referer={}",
            urlencoding::encode(&format!("list/{}", id))
        ))))
    }
}

async fn add_list(
    (state, user): (Arc<Mutex<State>>, String),
    form: AddListForm,
) -> Result<impl Reply, Rejection> {
    let mut state = state.lock().await;
    let t = random_token().map_err(|_| warp::reject::custom(Error::Internal))?;
    state
        .db
        .wishlists
        .insert(t, Wishlist::new(&user, &form.name));
    state
        .db
        .save()
        .map_err(|_| warp::reject::custom(Error::Internal))?;
    Ok(temporary_redirect("/"))
}

async fn remove_list(
    (state, user): (Arc<Mutex<State>>, String),
    id: String,
) -> Result<impl Reply, Rejection> {
    let mut state = state.lock().await;
    if let Some(list) = state.db.wishlists.get(&id) {
        if list.user == user {
            state.db.wishlists.remove(&id);
        } else {
            return Err(warp::reject::custom(Error::Forbidden));
        }
    }
    state
        .db
        .save()
        .map_err(|_| warp::reject::custom(Error::Internal))?;
    Ok(temporary_redirect("/"))
}

async fn add_item(
    (state, user): (Arc<Mutex<State>>, String),
    id: String,
    form: AddItemForm,
) -> Result<impl Reply, Rejection> {
    let mut state = state.lock().await;
    match state.db.wishlists.get_mut(&id) {
        Some(list) => {
            if list.user == user {
                let t = random_token().map_err(|_| warp::reject::custom(Error::Internal))?;
                list.items.insert(t, Item::new(&form.name));
            } else {
                return Err(warp::reject::custom(Error::Forbidden));
            }
        }
        None => return Err(warp::reject::not_found()),
    }
    state
        .db
        .save()
        .map_err(|_| warp::reject::custom(Error::Internal))?;
    Ok(temporary_redirect(&format!("/list/{}", id)))
}

async fn remove_item(
    (state, user): (Arc<Mutex<State>>, String),
    id: String,
    item_id: String,
) -> Result<impl Reply, Rejection> {
    let mut state = state.lock().await;
    match state.db.wishlists.get_mut(&id) {
        Some(list) => {
            if list.user == user {
                match list.items.get(&item_id) {
                    Some(_) => {
                        list.items.remove(&item_id);
                    }
                    None => return Err(warp::reject::not_found()),
                }
            } else {
                return Err(warp::reject::custom(Error::Forbidden));
            }
        }
        None => return Err(warp::reject::not_found()),
    }
    state
        .db
        .save()
        .map_err(|_| warp::reject::custom(Error::Internal))?;
    Ok(temporary_redirect(&format!("/list/{}", id)))
}

async fn buy_item(
    (state, user): (Arc<Mutex<State>>, String),
    id: String,
    item_id: String,
) -> Result<impl Reply, Rejection> {
    let mut state = state.lock().await;
    match state.db.wishlists.get_mut(&id) {
        Some(list) => {
            if list.user != user {
                match list.items.get_mut(&item_id) {
                    Some(i) => i.bought_by = Some(user),
                    None => return Err(warp::reject::not_found()),
                }
            } else {
                return Err(warp::reject::custom(Error::Forbidden));
            }
        }
        None => return Err(warp::reject::not_found()),
    }
    state
        .db
        .save()
        .map_err(|_| warp::reject::custom(Error::Internal))?;
    Ok(temporary_redirect(&format!("/list/{}", id)))
}

async fn unbuy_item(
    (state, user): (Arc<Mutex<State>>, String),
    id: String,
    item_id: String,
) -> Result<impl Reply, Rejection> {
    let mut state = state.lock().await;
    match state.db.wishlists.get_mut(&id) {
        Some(list) => {
            if list.user != user {
                match list.items.get_mut(&item_id) {
                    Some(i) => match &i.bought_by {
                        Some(u) => {
                            if u == &user {
                                i.bought_by = None;
                            } else {
                                return Err(warp::reject::custom(Error::Forbidden));
                            }
                        }
                        None => return Err(warp::reject::not_found()),
                    },
                    None => return Err(warp::reject::not_found()),
                }
            } else {
                return Err(warp::reject::custom(Error::Forbidden));
            }
        }
        None => return Err(warp::reject::not_found()),
    }
    state
        .db
        .save()
        .map_err(|_| warp::reject::custom(Error::Internal))?;
    Ok(temporary_redirect(&format!("/list/{}", id)))
}

async fn check_token(
    state: Arc<Mutex<State>>,
    token: Option<String>,
) -> Result<(Arc<Mutex<State>>, String), Rejection> {
    let s = state.lock().await;
    if let Some(u) = token.and_then(|t| s.tokens.get(&t)) {
        Ok((state.clone(), u.to_string()))
    } else {
        Err(warp::reject::custom(Error::Forbidden))
    }
}

async fn check_token_optional(
    state: Arc<Mutex<State>>,
    token: Option<String>,
) -> Result<(Arc<Mutex<State>>, Option<String>), Infallible> {
    let s = state.lock().await;
    let u = match token {
        Some(t) => match s.tokens.get(&t) {
            Some(x) => Some(x.to_string()),
            None => None,
        },
        None => None,
    };
    Ok((state.clone(), u))
}

fn with_state(
    state: Arc<Mutex<State>>,
) -> impl Filter<Extract = (Arc<Mutex<State>>,), Error = Infallible> + Clone {
    warp::any().map(move || state.clone())
}

async fn recover(err: Rejection) -> Result<impl Reply, Rejection> {
    error!("{:?}", err);
    let (error, code) = if let Some(&error) = err.find::<Error>() {
        (format!("{}", error), error.status())
    } else if let Some(error) = &err.find::<warp::reject::InvalidQuery>() {
        (format!("{}", error), StatusCode::OK)
    } else {
        (
            "Internal Server Error".to_string(),
            StatusCode::INTERNAL_SERVER_ERROR,
        )
    };
    let template = ErrorTemplate { error };
    match template.render() {
        Ok(body) => Ok(warp::reply::with_status(warp::reply::html(body), code)),
        Err(_) => Err(err),
    }
}

pub async fn run(db: Database, port: u16) {
    let state = Arc::new(Mutex::new(State {
        db,
        tokens: HashMap::new(),
    }));
    let index = warp::path::end()
        .and(with_state(state.clone()))
        .and(warp::cookie::optional("token"))
        .and(warp::query())
        .and_then(index);
    let debug = warp::path("debug")
        .and(with_state(state.clone()))
        .and_then(debug);
    let login = warp::path("login")
        .and(with_state(state.clone()))
        .and(warp::body::form())
        .and_then(login);
    let logout = warp::path("logout")
        .and(with_state(state.clone()))
        .and(warp::cookie::optional("token"))
        .and_then(logout);
    let css = warp::path("style.css").map(|| {
        warp::reply::with_header(
            include_str!("../../static/style.css"),
            "content-type",
            "text/css",
        )
    });
    let add_list = warp::path("add_list")
        .and(with_state(state.clone()))
        .and(warp::cookie::optional("token"))
        .and_then(check_token)
        .and(warp::body::form())
        .and_then(add_list);
    let remove_list = warp::path("remove_list")
        .and(with_state(state.clone()))
        .and(warp::cookie::optional("token"))
        .and_then(check_token)
        .and(warp::path::param::<String>())
        .and_then(remove_list);
    let list = warp::path("list")
        .and(with_state(state.clone()))
        .and(warp::cookie::optional("token"))
        .and_then(check_token_optional)
        .and(warp::path::param::<String>())
        .and_then(list);
    let add_item = warp::path("list")
        .and(with_state(state.clone()))
        .and(warp::cookie::optional("token"))
        .and_then(check_token)
        .and(warp::path::param::<String>())
        .and(warp::path("add"))
        .and(warp::body::form())
        .and_then(add_item);
    let remove_item = warp::path("list")
        .and(with_state(state.clone()))
        .and(warp::cookie::optional("token"))
        .and_then(check_token)
        .and(warp::path::param::<String>())
        .and(warp::path("remove"))
        .and(warp::path::param::<String>())
        .and_then(remove_item);
    let buy_item = warp::path("list")
        .and(with_state(state.clone()))
        .and(warp::cookie::optional("token"))
        .and_then(check_token)
        .and(warp::path::param::<String>())
        .and(warp::path("buy"))
        .and(warp::path::param::<String>())
        .and_then(buy_item);
    let unbuy_item = warp::path("list")
        .and(with_state(state.clone()))
        .and(warp::cookie::optional("token"))
        .and_then(check_token)
        .and(warp::path::param::<String>())
        .and(warp::path("unbuy"))
        .and(warp::path::param::<String>())
        .and_then(unbuy_item);

    let routes = warp::get()
        .and(
            index
                .or(debug)
                .or(logout)
                .or(css)
                .or(remove_list)
                .or(remove_item)
                .or(buy_item)
                .or(unbuy_item)
                .or(list),
        )
        .or(warp::post().and(login.or(add_list).or(add_item)))
        .recover(recover)
        .with(warp::log("wishlist"));

    warp::serve(routes).run(([0, 0, 0, 0], port)).await;
}
