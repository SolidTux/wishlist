# wishlist

Simple self-hosted wishlist tool.

## Features

* Multiple wishlists per user.
* Wishlist link can be shared with others who can select which items they
  already bought.
* Wishlists can only be viewed by logged in users.

## Installation

* [Install Rust](https://rustup.rs/)
* Build and install with `cargo install --path . --force`

## Usage

After the installation the `wishlish` command is available. Help is available
with `-h`/`--help`.

### User Management

The users can be managed with the subcommand `user`. The following features are
available currently

* Add user with `wishlist user add $USERNAME`
* Update user password with `wishlist user update $USERNAME`
* List users with `wishlist user list`
* Remove user with `wishlist user remove $USERNAME`

The server has to be restarted after each change. A webfrontend for user
management is not available and currently not planned.

### Server

The server is started with `wishlist server $PORT` where the port is 8080 if
omitted.

### Frontend

Each user only sees their own wishlists at first. To share a link with other,
just copy the link to the wishlist. If the wishlist is opened by another user,
the list is not editable anymore, but bought items can be marked.
